<?php

namespace App\Http\Controllers;
use App\Traits\ApiResponser;
use Validator;
use App\Models\LoanApplications;
use App\Models\LoanRepayments;
use Illuminate\Http\Request;

/**
 * [Description LoanAppController]
 */
class LoanAppController extends Controller
{
    // access trait for sending success and error response
    use ApiResponser;


    /**
     * Apply for loan
     * @return \Illuminate\Http\JsonResponse
     */
    public function newLoanApplication()
    {
        try{
            // used to validate the request , field amd its rulaes added in validation
            $validator = Validator::make(request()->all(), [
                'amount_required' => 'required|numeric',
                'loan_term'=> 'required|numeric'
                ]);
            // if validator fails return error message
            if ($validator->fails()) {
               return $this->error($validator->errors(),400);
            }

            //allow only the customer to create loan application
            $user = $this->checkValidUser('customer');
            if(!$user)
                return $this->error("Invalid user",403);
            
            // check pending loan is exist
            $pendingLoan = $this->checkPendingLoan();
            if($pendingLoan)
            {
                return $this->error("Unable to create new loan , old loan is still pending",200); 
            }
            //create the loan reference id for reference
            $reference_id = $this->createLoanReferenceId(); 
            $data = ['user_id'=>auth()->user()->id,'amount_required'=>request('amount_required'),'loan_term'=>request('loan_term'),
                        'reference_id'=>$reference_id,'status'=>'initiated'];
            
            // create the new loan application using above data
            $create = LoanApplications::create($data);
            if($create)
            {
                $message = "Your loan request submitted successfully. Loan reference id is ".$reference_id;
                return $this->success(NULL,$message,200);
            }
            else
            {
                return $this->error("Unable to submit loan request",200);
            }
        }
        catch(\Exception $e)
        {
            return $this->error($e->getMessage(),500);
        }   
    }


    /**
     * used to create the unique loan id
     * @return string
     */
    public function createLoanReferenceId()
    {
        $reference_id ="LA".rand('12345678',5);
        $loan_no = LoanApplications::where('reference_id',$reference_id)->first();
        if($loan_no)
            $this->createLoanReferenceId();
        else
            return $reference_id;
    }


    /**
     * Get loan list
     * @return \Illuminate\Http\JsonResponse
     */
    public function loanList()
    { 
        // check valid user to access this loan list
        $user = $this->checkValidUser('admin');
        if(!$user)
            return $this->error("Invalid user",403);

        try{
            // return all loan application
            $list = LoanApplications::get();
            if($list)
            {
                return $this->success($list,"loan list is available",200);
            }
            else
            {
                return $this->error("No new loan request",404);
            }
        }
        catch(\Exception $e)
        {
            return $this->error($e->getMessage(),500);
        }  
    }


    /**
     * return loan detail using ref id
     * @param $ref_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function loanDetail($ref_id)
    {
        try{
            $admin = $this->checkValidUser('admin');

            // check loan detail using ref id
            $detail = LoanApplications::where('reference_id',$ref_id)->first();
            if(!empty($detail))
            {
                if(!$admin && $detail->user_id != auth()->user()->id)
                {
                    return $this->success(NULL,"Invalid user",403);
                }

                return $this->success($detail,"loan detail is available",200);
            }
            else
            {
                return $this->error("Please enter valid loan reference id",404);
            }
        }
        catch(\Exception $e)
        {
            return $this->error($e->getMessage(),500);
        }  
    }


    /**
     * form the data with specific keys
     * @param $data
     * @return array
     */
    public function formList($data)
    {
        $list = array();
        $keys = array('user_id','status','required_amount','created_at');
        foreach($data as $key=>$value)
        {
            if(in_array($key,$keys))
                $list[$key] = $value;
        }
        return $list;
    }


    /**
     * Approve the loan
     * @param $ref_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function approveLoan($ref_id)
    {
        try{
            // check valid user to approve the loans
            $user = $this->checkValidUser('admin');
            if(!$user)
                return $this->error("Invalid user",403);
           
            // check loan detail using ref id
            $detail = LoanApplications::where('reference_id',$ref_id)->first();
            if(!empty($detail))
            {
                // change the application status from initiated to approved add remaining necessary details
                $data = $this->loanUpdate($detail);
                if($data)
                    return $this->success($detail,"loan approved successfully!",200);
                else    
                    return $this->success($detail,"sorry unable to approve this loan",400);
            }
            else
            {
                return $this->error("Please enter valid loan reference id",404);
            }
        }
        catch(\Exception $e)
        {
            return $this->error($e->getMessage(),500);
        }  
    }


    /**
     * Update the loan row
     * @param $detail
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function loanUpdate($detail)
    {
        try{
            $due_per_week = $this->loanDueAmount($detail->amount_required,$detail->loan_term); 
            $data = ['total_amount'=>$detail->amount_required,'total_repaid_amount'=>0,
            'approved_user_id'=>auth()->user()->id,'status'=>"approved","paid_dues"=>0,
            "due_per_week"=>$due_per_week];

            $update = $detail->update($data);
            if($update)
                return true;
            else
                return false;
        }
        catch(\Exception $e)
        {
            return $this->error($e->getMessage(),500);
        }
    }


    /**
     * @param $amount_required
     * @param $loan_term
     * @return float|int
     */
    public function loanDueAmount($amount_required, $loan_term)
     {
         // fetch the single due amount using the loan_term
        return $amount_required / $loan_term; 
    }


    /**
     * return loan detail like pending amount using ref id
     * @param $ref_id
     * @return array|\Illuminate\Http\JsonResponse
     */
    public function pendingLoanAmount($ref_id)
    {
        try{
            $data = array();

            // check loan detail using ref id and return details regarding the due
            $detail = LoanApplications::where('reference_id',$ref_id)->first();
            if(!empty($detail))
            {

                if($detail->user_id != auth()->user()->id)
                {
                  return $this->error("Unauthorized access",403);
                }
                $data['total_dues'] = $detail->loan_term;
                $data['total_paid_due'] = $detail->paid_dues;
                $data['total_pending_due'] = $detail->loan_term - $detail->paid_dues;
                $data['total_amount'] = $detail->total_amount;
                $data['total_repaid_amount'] = $detail->total_repaid_amount;
                $data['total_pending_amount'] = $detail->total_amount - $detail->total_repaid_amount;
                $data['reference_id'] = $ref_id;
                $data['status'] = $detail->status;
                return $data;
                return $this->success($detail,"loan detail is available",200);
            }
            else
            {
                return $this->error("Please enter valid loan reference id",404);
            }
        }
        catch(\Exception $e)
        {
            return $this->error($e->getMessage(),500);
        }  
    }


    /**
     * used to fetch the valid user
     * @param $role
     * @return bool
     */
    public function checkValidUser($role)
    {
        if(auth()->check() && auth()->user()->role== $role)
            return true;
        else
            return false;
    }

    /**
     * Check user have the pending loan
     * @return bool
     */
    public function checkPendingLoan()
    {
        $loan = LoanApplications::where('user_id',auth()->user()->id)->whereIn('status',['initiated','approved'])->get();
        if(count($loan)>0)
            return true;
        else
            return false;
    }
}
