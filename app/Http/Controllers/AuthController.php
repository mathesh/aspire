<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Validator;

class AuthController extends Controller
{
    use ApiResponser;


    /**
     * return the valid user token after register
     * @return \Illuminate\Http\JsonResponse
     */
    public function register()
    {
        // used to validate the request , field amd its rules added in validation
        $validator = Validator::make(request()->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:5|confirmed'
            ]);

        // if validator fails return error message
        if ($validator->fails()) {
            return $this->error($validator->errors(),400);

        }
        
        // creating the user 
        $user = User::create([
            'name' => request('name'),
            'password' => bcrypt(request('password')),
            'email' => request('email'),
            'role'=>'customer'
        ]);

        // if success authenctication return token for further communication
        // need to place this token in headers for secure and authenticated connection
        return $this->success([
            'token' => $user->createToken('API Token')->plainTextToken
        ]);
    }


    /**
     * return the logged in user token
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        // used to validate the request , field amd its rules added in validation
        $validator =Validator::make(request()->all(),[
            'email' => 'required|string|email|',
            'password' => 'required|string|min:5'
        ]);

        // if validator fails return error message
        if ($validator->fails()) {
            return $this->error($validator->errors(),400);
        }

        // try to authenticate using given credentials
        $data = array('email'=>request('email'),'password'=>request('password'));
        if (!Auth::attempt($data)) {
            return $this->error('Credentials not match', 401);
        }

        // if success authenctication return token for further communication
        // need to place this token in headers for secure and authenticated connection
        return $this->success([
            'token' => auth()->user()->createToken('API Token')->plainTextToken
        ]);
    }


    /**
     * used to logout user
     * @return array
     */
    public function logout()
    {
        // generate token will be deleted
        auth()->user()->tokens()->delete();

        return [
            'message' => 'Tokens Revoked'
        ];
    }


    /**
     * to access the authenticated user profile
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function profile()
    {
        return auth()->user();
    }
}