<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Traits\ApiResponser;
use Validator;
use App\Models\LoanApplications;
use App\Models\LoanRepayments;

class LoanRepaymentController extends Controller
{
    use ApiResponser;
    private $appController;

    /**
     * LoanRepaymentController constructor.
     * @param LoanAppController $appController
     */
    public function __construct(LoanAppController $appController)
    {
        $this->appController = $appController;

    }

    /**
     * Repay the loan due
     * @return \Illuminate\Http\JsonResponse
     */
    public function repayLoan()
    {
        try{

        // check valid user to access loan detail using ref id
        $validator = Validator::make(request()->all(), [
            'reference_id' => 'required',
            'due_amount' =>'required|numeric'
            ]);

         // if validator fails return error message
        if ($validator->fails()) {
            return $this->error($validator->errors(),400);
        }
        // check valid user using existing function available from LoanAppController
        $user = $this->appController->checkValidUser('customer');
        if(!$user)
            return $this->error("Invalid user",403);

        $ref_id = request('reference_id');
        $due_amount = request('due_amount');

        // check the loan detail with multiple conditions
        $loan = $this->loanDetail($ref_id);
        // if status is false send error message 
        if($loan['status'] == false)
            return $this->error($loan['message'],404);

        // check total paid due already   
        $repaid = LoanRepayments::where('loan_reference_id',$ref_id)->sum('paid_due');
        $repaid_amount = (isset($repaid))? $repaid : 0;

        // create new loan receipt id for paid dues
        $receipt_no = $this->createLoanReceiptId();

        // form the array to insert data in db
        $data = ['repaid_amount'=>$repaid_amount + $due_amount,'user_id'=>auth()->user()->id,'paid_due'=>$due_amount,
        'loan_reference_id'=>$ref_id,'receipt_no'=>$receipt_no];
        $create = LoanRepayments::create($data);
        if($create)
        {
            // update the paid amount in LoanApplications table
            $update_loan = $this->updateLoanAmount($ref_id);
            return $this->success(NULL,"Loan Due paid successfully. Your receipt no is ".$receipt_no,200);
        }
        else
        {
            return $this->error("Unable to submit loan request",500);
        }
    }
    catch(\Exception $e)
    {
        return $this->error($e->getMessage(),500);
    }   
        
    }


    /**
     * return loan detail using ref id
     * @param $ref_id
     * @return array
     */
    public function loanDetail($ref_id)
    {
        try{
            $data = [];
            // fecth loan using reference id
            $detail = LoanApplications::where('reference_id',$ref_id)->first();
            if(!empty($detail))
            {
                // check same user access the data
                if($detail->user_id != auth()->user()->id)
                {
                    $data['message'] = 'Unauthorized access';
                    $data['status'] = false;
                    return $data;
                }

                // check the loan is approved
                if($detail->status!='approved')
                {
                    $data['message'] = 'Your loan application is still in progress!';
                    $data['status'] = false;
                    return $data;
                }

                // check for exisiting due for weekly repaid calculation
                $repayment = LoanRepayments::where('loan_reference_id',$ref_id)->orderBy('id','desc')->first();
                if(!empty($repayment))
                {
                $last_paid = $repayment->created_at;
                $checkFrequency = $this->checkWeekly($last_paid);
                if($checkFrequency)
                {
                    $prev_paid = strtotime($last_paid);
                    $next_due = strtotime("+7 day", $prev_paid);
                    $next_due =  date('d-m-Y', $next_due);
                    $data['message'] = 'Your next due is '. $next_due;
                    $data['status'] = false;
                    return $data;
                }
                }
                // check the due amount should match the due_per_week
                if($detail->due_per_week > request('due_amount') || request('due_amount') > $detail->due_per_week)
                {
                    $message = "Due amount should be equal to " . $detail->due_per_week;
                    $data['message'] = $message;
                    $data['status'] = false;
                    return $data;

                }
                else
                {
                    // check the loan status
                    $check = $this->checkLoanClosed($ref_id);
                    if($check)
                    {
                        $data['message'] = 'Your loan is closed already!';
                        $data['status'] = false;
                        return $data;
                    }
                }
                $data['message'] = 'ok';
                $data['status'] = true;
                return $data;
            }
            else
            {
                // if invalid reference raise the error
                $data['message'] = 'Please enter valid loan reference id';
                $data['status'] = false;
                return $data;

            }
        }
        catch(\Exception $e)
        {
            dd($e);
            $data['message'] =  $e->getMessage();
            $data['status'] = false;
            return $data;
        }  
    }


    /**
     * update the loan amount
     * @param $ref_id
     */
    public function updateLoanAmount($ref_id)
    {
        $detail = LoanApplications::where('reference_id',$ref_id)->first();
        $detail->total_repaid_amount += request('due_amount');
        $detail->paid_dues += 1;
        if($detail->total_repaid_amount==$detail->total_amount)
            $detail->status ="closed";
        $detail->save();
    }


    /**
     * check the loan status using reference id
     * @param $ref_id
     * @return bool
     */
    public function checkLoanClosed($ref_id)
    {
        $detail = LoanApplications::where(['reference_id'=>$ref_id,'status'=>'closed'])->first();
        if($detail)
           return true;
        else
           return false;
    }


    /**
     * used to create the unique receipt no
     * @return string
     */
    public function createLoanReceiptId()
    {
        $receipt_no ="RE".rand('12345678',3);
        $receipt = LoanRepayments::where('receipt_no',$receipt_no)->first();
        if($receipt)
            $this->createLoanReceiptId();
        else
            return $receipt_no;
    }


    /**
     * check the weekly frequency the loan is paid
     * @param $date
     * @return bool
     */
    public function checkWeekly($date)
    {
        $s_ref= $date->format('Y-m-d h:i:s');
        $s = date('Y-m-d h:i:s');
        $d_ref = new \DateTime($s_ref);
        $d = new \DateTime($s);
    
        $dow = $d_ref->format('w');
        $delta = ($dow == 6)?7:(6 - $dow);
    
        $d_ref->modify("+$delta days midnight");
        return $d < $d_ref;
    }
}
