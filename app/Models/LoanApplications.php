<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanApplications extends Model
{
    use HasFactory;

    protected $fillable = ["user_id","amount_required","total_amount","total_repaid_amount","loan_term",
                            "total_due","approved_user_id","reference_id","status","total_dues","due_per_week","paid_dues"];
}
