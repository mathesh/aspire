<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LoanRepayments extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','paid_due','repaid_amount','loan_reference_id','receipt_no'];
}
