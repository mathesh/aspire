<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('loan_applications');

        Schema::create('loan_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('amount_required');
            $table->bigInteger('total_amount')->nullable();
            $table->bigInteger('total_repaid_amount')->nullable();
            $table->bigInteger('loan_term');
            $table->integer('paid_dues')->nullable();
            $table->integer('due_per_week')->nullable();
            $table->bigInteger('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->bigInteger('approved_user_id')->unsigned()->index()->nullable();
            $table->foreign('approved_user_id')->references('id')->on('users')->onDelete('cascade');           
            $table->string('reference_id');
            $table->unique('reference_id');
            $table->text('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_applications');
    }
}
