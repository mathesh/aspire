<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LoanAppController;
use App\Http\Controllers\LoanRepaymentController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/auth/register', [AuthController::class, 'register']);

Route::post('/auth/login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/me',[AuthController::class, 'profile']);



    Route::group(['prefix'=>'loan'],function()
    {
        /* Admin route */
        Route::get('request/list',[LoanAppController::class,"loanList"]);
        Route::get('detail/{id}',[LoanAppController::class,"loanDetail"]);
        Route::patch('approve/{id}',[LoanAppController::class,"approveLoan"]);
        /* Customer route */
        Route::post('apply',[LoanAppController::class,"newLoanApplication"]);
        Route::post('repay',[LoanRepaymentController::class,"repayLoan"]);
        Route::get('due/{id}',[LoanAppController::class,"pendingLoanAmount"]);
    });

    Route::post('/auth/logout', [AuthController::class, 'logout']);
});