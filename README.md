# Mini Aspire Simple Loan Application

It is a demo project for demonstrating of Mini Aspire for simple loan application in Laravel. 

## How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate --seed__ (it has some seeded data for your testing)
- That's it: launch the API , refer the API documentation and for admin login use default credentials __admin@admin.com__ - __admin__
- For customer login use default credentials __mathesh@gmail.com__ - __qwerty__

## License

Basically, feel free to use and re-use any way you want.

---